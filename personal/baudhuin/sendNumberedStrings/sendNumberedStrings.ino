/*
 * Simple utility program sending numbered strings on the Serial interface.
 * Used for testing the transmission.
 */

#define DEBUG
#include "DebugCSPU.h"

uint16_t counter=0;

void setup() {
  DINIT(115200);
  Serial << F("Setup ok") << ENDL;
}

void loop() {
  Serial <<  counter++ << ": A dummy string" << ENDL;
  delay(1000);
}
