/*
   EEPROM_BankWriter.cpp
*/

#define DEBUG
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "EEPROM_BankWriter.h"

#define DBG_INIT 0
#define DBG_INIT_HEADER_INFO 0
#define DBG_READ_WRITE 0
#define DBG_ERASE 0
#define DBG_STORE_DATA 0
#define DBG_REPORT_MEM_FULL 1
#define DBG_MAINTENANCE 0
#define DBG_OPERATION 1
#define DBG_NEXT_CHIP 0

#ifdef USE_COUT
#include <iostream>
using namespace std;
#endif
#include "ExtEEPROM.h"


EEPROM_BankWriter::EEPROM_BankWriter(const byte theMaintenancePeriodInSec) {
  flags.initialized = false;
  hardware = nullptr;
  flags.headerToBeUpdated = false;
  flags.memoryFull = false;
  timeElapsedSinceMaintenance = 0;
  firstEEPROM_Chip = 0;
  maintenancePeriodInSec = theMaintenancePeriodInSec;
}

EEPROM_BankWriter::~EEPROM_BankWriter() {

}

bool EEPROM_BankWriter::init(const EEPROM_Key theKey, const HardwareScanner &hw, const byte recordSize)
{
  DASSERT(flags.initialized == false);
  if (hw.getNumExternalEEPROM() == 0) return false;
  flags.initialized = initHeaderInfo(theKey, hw, recordSize);
  if (!flags.initialized) {
    DPRINTSLN(DBG_OPERATION, "EEPROM_Bank init failed.");
  }
  else
  {
    DPRINTS(DBG_OPERATION, "EEPROM_Bank ok (Total:");
    DPRINT(DBG_OPERATION, getTotalSize());
    DPRINTS(DBG_OPERATION, " bytes/free: ");
    DPRINT(DBG_OPERATION, getFreeSpace());
    DPRINTSLN(DBG_OPERATION, " bytes)");
  }
  return flags.initialized;
}

void EEPROM_BankWriter::doIdle() {
  if (timeElapsedSinceMaintenance < maintenancePeriodInSec * 1000) return;

  if (flags.headerToBeUpdated) {
    writeHeader(hardware->getExternalEEPROM_I2C_Address(firstEEPROM_Chip));
    DPRINTSLN(DBG_MAINTENANCE, "HeaderUpdated");
    flags.headerToBeUpdated = false;
  }
  timeElapsedSinceMaintenance = 0;
}

bool EEPROM_BankWriter::noMoreChip() const {
  return (header.firstFreeChip == (header.numChips - 1));
}

bool EEPROM_BankWriter::goToNextChip() {
  DASSERT(header.firstFreeChip < header.numChips);
  byte next = nextChip(header.firstFreeChip);
  if (next != 0) {
    header.firstFreeChip = next;
    flags.headerToBeUpdated = true;
    return true;
  }
  else {
    flags.memoryFull = true;
    return false;
  }
}

// Return the sequence number of the next chip, if any, 0 if none.
byte EEPROM_BankWriter::nextChip(const byte current) {
  byte candidate = current + 1;
  if (current >= header.numChips) {
    // No next chip found.
    DPRINTSLN(DBG_NEXT_CHIP, "No next chip found");
    flags.memoryFull = true;
    candidate = 0;
  }
  else {
    DPRINTS(DBG_NEXT_CHIP, "Now in chip #");
    DPRINTLN(DBG_NEXT_CHIP, candidate);
  }

  return candidate;
}

bool EEPROM_BankWriter::initHeaderInfo(EEPROM_Key headerKey, const HardwareScanner &hw, byte recordSize)
{
  DASSERT(hw.getNumExternalEEPROM() > 0);

  // Find first chip with valid header, set firstEEPROM_Chip
  firstEEPROM_Chip = 0;
  byte I2C_Address;
  bool headerFound = false;
  unsigned int chipLastAddress = hw.getExternalEEPROM_LastAddress(0);
  byte numChips = hw.getNumExternalEEPROM();
  // Scan every chip until a valid header is found.
  // After this, firstEEPROM_Chip is the one containing a header,
  // or, if none found, the first available one.
  int i = 0;
  while ( (i < numChips) && (!headerFound) ) {
    // Currently assume every chip has same size as first one.
    DASSERT(chipLastAddress == hw.getExternalEEPROM_LastAddress(i));
    I2C_Address = hw.getExternalEEPROM_I2C_Address(i);
    readHeader(I2C_Address);
    headerFound = (header.headerKey == headerKey);
    if (headerFound) firstEEPROM_Chip = i;
    i++;
  } // while

  DPRINTS(DBG_INIT_HEADER_INFO, "First EEPROM is chip #");
  DPRINTLN(DBG_INIT_HEADER_INFO, firstEEPROM_Chip);

  if (!headerFound) {
    I2C_Address = hw.getExternalEEPROM_I2C_Address(firstEEPROM_Chip);
    // No header found, create one.
    DPRINTSLN(DBG_INIT_HEADER_INFO, "Header not found.");
    DPRINTS(DBG_INIT_HEADER_INFO, "Creating header in EEPROM at 0x");
    DPRINTLN(DBG_INIT_HEADER_INFO, (int) I2C_Address, HEX);

    header.headerKey = headerKey;
    header.numChips = numChips;
    header.firstFreeByte = sizeof(header);
    header.firstFreeChip = firstEEPROM_Chip;
    header.chipLastAddress = chipLastAddress;
    header.recordSize = recordSize;
    writeHeader(I2C_Address);
  }
  else {
    DPRINTSLN(DBG_INIT_HEADER_INFO, "Header found.");
    readHeader(hw.getExternalEEPROM_I2C_Address(firstEEPROM_Chip)); // reload the real header.
    // Check chip size is consistent with provided one.
    if ((header.chipLastAddress != chipLastAddress)) {
      DPRINTS(DBG_OPERATION, "Header chip last address=");
      DPRINT(DBG_OPERATION, header.chipLastAddress);
      DPRINTS(DBG_OPERATION, ", init chip last address=");
      DPRINT(DBG_OPERATION, chipLastAddress);
      DPRINTSLN(DBG_OPERATION, ": cannot initialize without losing data.");
      return false;
    }
    if (header.numChips > numChips-firstEEPROM_Chip) {
      DPRINTS(DBG_OPERATION, "Header numChips=");
      DPRINT(DBG_OPERATION, header.numChips);
      DPRINTS(DBG_OPERATION, ", actual=");
      DPRINT(DBG_OPERATION, numChips-firstEEPROM_Chip);
      DPRINTSLN(DBG_OPERATION, ": cannot initialize without losing data.");
      return false;
    }
    // Check record size is consistent with provided one.
    if ((header.recordSize != recordSize)) {
      DPRINTS(DBG_OPERATION, "Header recordSize=");
      DPRINT(DBG_OPERATION, header.recordSize);
      DPRINTS(DBG_OPERATION, ", init recordSize=");
      DPRINT(DBG_OPERATION, recordSize);
      DPRINTSLN(DBG_OPERATION, ": cannot initialize without losing data.");
      return false;
    }
  }
  // Initialize any other info.
  hardware = &hw;
  return true;
}


// This method supports variable size EEPROMs and takes unavailable chips into account.
unsigned long EEPROM_BankWriter::getTotalSize() const {
  DASSERT(flags.initialized);
  unsigned long size = 0;
  for (byte i = 0; i < header.numChips; i++) {
    size += hardware->getExternalEEPROM_LastAddress(i);
    size++;  // size = lastAddress + 1
  }
  return size;
}

// This method supports variable size EEPROMs.
unsigned long EEPROM_BankWriter::getFreeSpace() const {
  DASSERT(flags.initialized);
  unsigned long size = 0;
  for (byte i = header.firstFreeChip; i < header.numChips; i++) {
    size += hardware->getExternalEEPROM_LastAddress(i);
    size++; //size = lastAddress + 1;
  }
  size -= header.firstFreeByte;

  // NB: Header size is always either included in firstFreeByte, either included in a full chip.
  return size;
}

void EEPROM_BankWriter::readHeader(const byte EEPROM_I2C_Address) {
  DPRINTS(DBG_READ_WRITE, "EEPROM_BankWriter::readHeader from ");
  DPRINTLN(DBG_READ_WRITE, EEPROM_I2C_Address);
  byte read = readFromEEPROM(EEPROM_I2C_Address, 0, (byte *) &header, sizeof(EEPROM_Header));
  DASSERT(read == sizeof(EEPROM_Header));
}

void EEPROM_BankWriter::writeHeader(const byte EEPROM_I2C_Address) {
  DPRINTSLN(DBG_READ_WRITE, "EEPROM_BankWriter::writeHeader");
  byte written = writeToEEPROM( EEPROM_I2C_Address, 0, (byte *) &header, sizeof(EEPROM_Header));
  DASSERT(written == sizeof(EEPROM_Header));
  flags.headerToBeUpdated = false;
}

/* Read data. 32 - sizeof(address) bytes is the max size allowed by underlying Wire library  */
unsigned int EEPROM_BankWriter::readFromEEPROM(const byte EEPROM_I2C_Address, EEPROM_Address address, byte * buffer, const byte size) const {
  DASSERT(size <= 32 - sizeof(address)); // Do not exceed Wire library internal buffer
#if DBG_READ_WRITE==1
  char str[70];
  sprintf(str, "*** ReadFromEEPROM(I2C_Address=0x%02x, address=0x%04x, %d bytes.", EEPROM_I2C_Address, address, size);
  Serial.println(str);
  sprintf(str, "buffer address: 0x%04x", (unsigned int) buffer);
  Serial.println(str);
  // Do not dump buffer here: it does not contain anything useful.
#endif
  return ExtEEPROM::readData(EEPROM_I2C_Address, address, buffer, size);
}

/* write data. 32 - sizeof(address) bytes is the max size allowed by underlying Wire library */
unsigned int EEPROM_BankWriter::writeToEEPROM(const byte EEPROM_I2C_Address, EEPROM_Address address, const byte * const buffer, const byte size) {
#if DBG_READ_WRITE==1
  char str[10];
  Serial << F("*** writeToEEPROM(I2C_Address=0x");
  sprintf(str, "%02x", EEPROM_I2C_Address);
  Serial.print(str);
  Serial << F(", address=0x");
  sprintf(str, "%04x", address);
  Serial.print(str);
  Serial << size << F(" bytes") << ENDL;
  Serial << F("data (hex):");
#endif
  return ExtEEPROM::writeData(EEPROM_I2C_Address, address, buffer, size);
}


bool EEPROM_BankWriter::storeOneRecord(const byte* data, const byte dataSize) {
  DASSERT(header.recordSize == dataSize);  // Currently only support fixed-size records.
  return storeData(data, dataSize);
}

bool EEPROM_BankWriter::storeData(const byte* data, const byte dataSize) {
  DPRINTSLN(DBG_STORE_DATA, "Store one record IN");
  if (memoryFull()) {
    return false;
  }

  bool result = false;
  byte currentChip = firstEEPROM_Chip + header.firstFreeChip;

  // Check there is dataSize free bytes in current chip.
  // Never add anything to firstFreeByte or chipLastAddress: 
  // it could exceed the max. value of an EEPROM_Address.
  unsigned long firstFreeByteAfterWrite = ((unsigned long) header.firstFreeByte) + dataSize;
  if (firstFreeByteAfterWrite-1 <= (header.chipLastAddress)) {
    // Data fits in current chip.
    DPRINTSLN(DBG_STORE_DATA, "Writing inside current chip");
    writeToEEPROM(hardware->getExternalEEPROM_I2C_Address(currentChip), header.firstFreeByte, data, dataSize);
    if ( firstFreeByteAfterWrite-1 == (((long)  header.chipLastAddress))) {
      DPRINTSLN(DBG_STORE_DATA, "Entering next chip with firstFreeByte");
      // skip malfunctionning chip
      goToNextChip(); // This sets the memoryFull flag, if required and prints diagnostic.
      firstFreeByteAfterWrite -= (((long) header.chipLastAddress) + 1);
      DASSERT(firstFreeByteAfterWrite == 0);
    }
    header.firstFreeByte = firstFreeByteAfterWrite;
    result = true;
  } else {
    // Data at least partly in the next chip (if any)
    DPRINTSLN(DBG_STORE_DATA, "Writing past current chip");
    long firstPart = (long) header.chipLastAddress - (long) header.firstFreeByte + 1;
    DPRINTS(DBG_STORE_DATA, "First part:");
    DPRINTLN(DBG_STORE_DATA, (long) firstPart);
    if (firstPart > 0) {
      if (noMoreChip()) {
        DPRINTSLN(DBG_REPORT_MEM_FULL, "EEPROM: Not enough memory.");
      }
      else {
        writeToEEPROM(  hardware->getExternalEEPROM_I2C_Address(currentChip),
                        header.firstFreeByte, data, firstPart);
        header.firstFreeByte += firstPart;
        header.firstFreeByte -= (header.chipLastAddress + 1);
        DASSERT(header.firstFreeByte == 0);
        bool success = goToNextChip(); // This will succeed otherwise noMoreChip() would have returned true.
        DASSERT(success);
        writeToEEPROM(hardware->getExternalEEPROM_I2C_Address(firstEEPROM_Chip + header.firstFreeChip),
                      header.firstFreeByte, &data[firstPart], dataSize - firstPart);
        header.firstFreeByte += (dataSize - firstPart);
        result = true;
      }
    } // else
  }
  flags.headerToBeUpdated = true;
  DPRINTS(DBG_STORE_DATA, "firstFreeByte at end: 0x");
  DPRINTLN(DBG_STORE_DATA, (int) header.firstFreeByte, HEX);
  return result;
}

void EEPROM_BankWriter::erase() {
	flags.initialized = false;

	// Overwrite header key in any available chip, to avoid reuse.
	unsigned int invalidKey = 0;
	const HardwareScanner * hardware=getHardware();
	for (byte i = 0; i < hardware->getNumExternalEEPROM(); i++)
	{
		byte address = hardware->getExternalEEPROM_I2C_Address(i);
		writeToEEPROM(address, 0, (byte*) &invalidKey, sizeof(invalidKey));
		DPRINTS(DBG_ERASE, "Overwriting header at I2C 0x");
		DPRINTLN(DBG_ERASE, address, HEX);

	}
	init(header.headerKey, *hardware, header.recordSize);
}

void EEPROM_BankWriter::eraseFrom(byte chipId, EEPROM_Address addressInChip)
{
  DASSERT(chipId <= header.numChips)
  header.firstFreeByte= addressInChip;
  header.firstFreeChip= chipId;
  writeHeader(hardware->getExternalEEPROM_I2C_Address(firstEEPROM_Chip));
}

