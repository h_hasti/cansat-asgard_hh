#include "BlinkingLed.h"  //Include the headers that contain our objects
#include "SOSLed.h"       //
//Declaration of the objects
BlinkingLed led1(12, 1000);
SOSLed led2(10, 2000);
void setup() {
  // put your setup code here, to run once:
}

void loop() {
  // put your main code here, to run repeatedly:
  led1.run();   //Call the functions
  led2.run();   //

}
