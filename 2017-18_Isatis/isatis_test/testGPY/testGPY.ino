#include "GPY_Calculator.h"
#include "IsatisHW_Scanner.h"
#include "output.h"
#include "optimize.h"
#include "common.h"

const byte TestOperational = 1;
const byte Optimize        = 2;
const byte Calibrate       = 3;
const byte Stop            = 4;      // always keep last.

byte chooseTestType() {
  __FlashStringHelper* answers[Stop] = {
      F("Test with operational settings"), 
      F("Optimize setings"), 
      F("Compute Voc calibration value (after other settings are frozen)"),  
      F("Exit program")};
  return selectFrom(F("Select operation"), answers, Stop);
}

void calibrateSensor() {

  Serial << ENDL;
  int refDensity = 0;
  while (Serial.available()) Serial.read();
  Serial << F("Enter expected dust density in µg/m3 at test location (integer, >0): ");
  Serial.flush();
  while (refDensity == 0 )   refDensity = Serial.parseInt();

  Serial << refDensity << F(" µg/m3") << ENDL;
  bool fan = selectYN(F("Use fan while measuring? "));
  resetGlobals();
  GPY::setFanOn(fan);  
  GPY_Calculator::Voc=3.0;
  Serial << F("Voc set to 3.0V") << ENDL;
  Serial << F("Performing 100 readings to adjust Voc assuming a 0 µG/m3 density") << ENDL;
  for (int i = 1; i <=100 ; i++)
  {
    performOneReading(i);
  }
  Serial << F("Stabilized Voc: ")<< GPY_Calculator::Voc << F("V") << ENDL;
  float correction = ((float) refDensity)/GPY_SensitivityInMicroGramPerV;
  Serial << F("Correction to match ") << refDensity << F("µC/m3 = ") << correction << ENDL;
  float calibVoc=GPY_Calculator::Voc - correction;
  Serial << F("Resulting recommanded Voc = ") << calibVoc << F(" V") << ENDL;
  Serial << F("Performing 100 readings with this Voc value to check calibration: ") << ENDL;
  resetGlobals();
  GPY_Calculator::Voc = calibVoc;
  for (int i = 1; i <=100 ; i++)
  {
    performOneReading(i);
  }
  printResults(Serial);
  Serial << ENDL;
  float densityAbsoluteError=GPY_MinSignificantDeltaV*GPY_SensitivityInMicroGramPerV;
  float totalExpectedError=0.1*refDensity + densityAbsoluteError;
  float averageDensity=totalOkDensity / numOK_Readings ;
  float actualError=fabs(averageDensity - refDensity);
  Serial << F("Expected accuracy= 10% +") <<densityAbsoluteError << F(" µG/m3") << ENDL;
  Serial << F("Expected total error for ") << refDensity << F(" µG/m3 = ") << totalExpectedError << F(" µG/m3") << ENDL;
  Serial << F("Actual error: ") << actualError << F(" µG/m3") << ENDL;
  Serial << F("Average measured value (") << averageDensity;
  if (actualError <= totalExpectedError) {
     Serial << F(" µg/m3) is within expected range") << ENDL;
     Serial << F("*** Update IsatisConfig.h with GPY_DefaultVocValue=") <<calibVoc << F(" V ***") << ENDL;
  }
  else  Serial << F("µg/m3) is NOT within expected range :-(") << ENDL;
 
  Serial << F("Calibration over.") << ENDL<<ENDL;
  GPY::setFanOn(false);
}

void testWithOperationalValues() {
  resetGlobals();
  GPY::setFanOn(useFan);
  delay(500);
  Serial << F("Performing ") << numReadings << F(" readings of GPY sensor.") << ENDL;
  Serial << ENDL << ENDL;
  Serial << F("#, OutputV (quality), Vco, Dust density") << ENDL;
//  Serial1 << ENDL << ENDL;
//  Serial1 << F("#, OutputV (quality), Vco, Dust density") << ENDL;

  for (unsigned int i = 1 ; i <= numReadings ; i++) {
    performOneReading(i);
  }
  GPY::setFanOn(false);
  printContext(Serial);
//  printContext(Serial1);
  delay(200);
  printResults(Serial);
//  printResults(Serial1);
  GPY::setFanOn(false);
}

void setup() {
  DINIT(9600);  // DO NOT INCREASE TO 115200: TMinus µC fails to parseInt at 115200 (reported to TMinus) ! 
  IsatisHW_Scanner hw;
  hw.IsatisInit();
  hw.printFullDiagnostic(Serial);
  delay(100);
//  Serial1.begin(serial1BaudRate);
//  while (!Serial1) ;
//  hw.printFullDiagnostic(Serial1);
  delay(100);
  printContext(Serial);
//  delay(100);
//  printContext(Serial1);
  if (ignoreFirstReading)
  {
    Serial << F("Performing a reading and ignoring it ");
    performOneReading(1);
    Serial << ENDL;
  }
}

void loop() {
  Serial << ENDL;
  byte choice = chooseTestType();
  switch (choice) {
    case TestOperational:
      testWithOperationalValues();
      break;
    case Optimize:
      optimizeSettings();
      break;
    case Stop:
      Serial << "---- Program terminated ----" << ENDL;
      Serial.flush();
      exit(-1);
      break;
    case Calibrate:
      calibrateSensor();
      break;
    default:
      DASSERT(false);
  } // switch
} // loop
