/*
   Test program for DebugCSPU library

   Simulates tracing in 2 different source files and 2 external libraries.
   See On-board Sofware Design Document for test instructions.

*/

#include "tst_cansat_config.h"
#include "tst_cansat_src2.h"

#include "test_lib1.h"
#include "test_lib2.h"
#include "test_simpleLib.h"

// ------------------------- MAIN FUNCTIONS ---------------------------------
void setup() {
#ifdef INIT_SERIAL
  DINIT(9600); // Init debugging
#endif
  // All following lines are noop if DEBUG is not defined.
  DFREE_RAM(DBG_SETUP);
  pinMode(LED_BUILTIN, OUTPUT);
  DPRINTSLN(DBG_SETUP, "setup OK");
  //NB: do not use the F macro: it is automatically used inside the DebugCSPU library
}

void loop() {
  DPRINTSLN(DBG_LOOP, "--------------------");
  DPRINTSLN(DBG_LOOP, "**loop in **");   

  DPRINTSLN(DBG_LOOP, "   Printing float (531.531), without and with a carriage return after the numeric value, with default format and with 5 decimal positions: ");
  DPRINT(DBG_LOOP, 531.531);
  DPRINTS(DBG_LOOP, " ");
  DPRINT(DBG_LOOP, 531.531, 5);
  DPRINTS(DBG_LOOP, " ");
  DPRINTLN(DBG_LOOP, 531.531, 5);


  DPRINTS(DBG_LOOP, "   Printing integer (254), in decimal and hexadecimal format : ");
  DPRINT(DBG_LOOP, 254, DEC);
  DPRINTS(DBG_LOOP, "=0x");
  DPRINTLN(DBG_LOOP, 254, HEX);

#ifdef DO_SOMETHING_LOCAL
  doSomething();  // Call function in other source file within same sketch
#endif
#ifdef CALL_LIBRARIES
  dummySimpleLibFunction(); // Call function from single file library
  dummyLibFunctionFromSrc1(); // Call library functions from 2 different files
  dummyLibFunctionFromSrc2();
#endif

  DDELAY(DBG_LOOP, 2000); // wait for an additional 2 secs  if debugging

  DASSERT(2 > 1);
  DPRINTSLN(DBG_LOOP, "Assert 1 OK");

#ifdef TEST_FAILED_ASSERTION   // Program should not stop if not debugged.
  DPRINTSLN(1, "Assertion should fail:");
  DASSERT(1 > 2);
#endif
  // This should never be printed, since program is aborted by the assertion when debug is on.
  DPRINTSLN(DBG_LOOP, "Exiting loop");
  DPRINTSLN(DBG_LOOP, "**loop out **");
}
