/* 
   DebugCSPU.h
  */
 /** @file
    @brief Main header for the Common debugging macros and utilities library.

    DebugCTIV.h includes all other header files except Timer.h, and should be the only one explicitely included,
    unless timers are used..
    Be sure to (un)define USE_ASSERTION and DEBUG to include the actual features in your test executable
    (and be sure not to include them in your operation executables).

    See library documentation of usage information. 
 *******************************************************************************************/
 
 /** @file */ 
 /** @defgroup DebugCSPU DebugCSPU library 
 *  @brief  Common debugging macros and utilities any Arduino project.
 *  
 *  The DebugCSPU library contains various objects, macro-definitions etc to support tracing & debugging 
 *  on the serial interface, with a zero memory foot-print in the operational software.
 *  
 *  Tracing can be enabled globally by defining the DEBUG preprocessor symbol, and enabled specifically 
 *  for part of a source file by defining custom  preprocessor. Each output macro will only result in 
 *  debugging code if the associated custom symbol is defined to a non-null integer.
 *  This allows for specifically turning on or off the debugging instructions for a specific part of the 
 *  software: typically, use a symbol like DBG_XXXX for the debugging of a particular feature, possibly 
 *  even using DBG_XXXX for tracing the main steps of the feature, and DBG_XXXX_VERBOSE for generating 
 *  a more detailed output.
 *  
 *  When the main DEBUG symbol is undefined, the memory footprint of the library is zero. This removes 
 *  any need to manually strip the final code from debugging instructions (nevertheless, the library 
 *  could be the only reason to initialize the serial interface: be sure to avoid initializing it 
 *  if no debugging is enabled anywhere). 
 *  
 *  When the main DEBUG symbol is defined, the strings passed to the various debugging macros are 
 *  stored in program memory.
 *  
 *  In addition to the tracing macros, the library provides support for assertions in a way that allows 
 *  for stripping out  the overhead code completely.  It does not use  the standard features provided 
 *  by <assert.h> which result in about 120 bytes in dynamic memory due to globals as soon 
 *  as __ASSERT_USE_STDERR is defined). 
 *  
 *  When symbol USE_ASSERTIONS is defined, only minimal contexte information is  maintained but still 
 *  results in significant overhead in program memory.  When symbol USE_ASSERTIONS is not defined, 
 *  DASSERT() results in 0 memory overhead.
 *  
 *  
 *  This library also includes:
 *  @li File VirtualMethods.h which defines macros VIRTUAL, VIRTUAL_FOR_TEST and NON_VIRTUAL. They can be 
 *  used to replace the ‘virtual’ keyword in class header files in order to have some methods virtual 
 *  in the test environment and not in the operational environment (select by defining the 
 *  USE_TEST_VIRTUAL_METHODS symbol during the build). This allows for overloading some methods in 
 *  test subclasses required to simulate specific conditions, while avoiding to vtable overhead in 
 *  the final software.
 *  @li File StringStream.h which provides a very useful class for streaming into a string buffer (it is 
 *  used  e.g. to output the acquired data in CSV format for storage on the SD Card. 
 *  @li Files Timer.h and Timer.cpp which provides a utility class and macro for profiling code. 
 *  @li The NULL definition (if not provided anywhere else), since std::nullptr is not 
 *  compatible with Arduino v1.6 (see #NULL) and the NULL definition is in stddef.h, which has a significant
 *  memory footprint. 
 *  
 *  @par Usage
 *  Only include header DebugCSPU.h, which includes any other useful header except Timer.h. 
    @code
    #define USE_ASSERTION // Comment this line out for operational software
    #define DEBUG
    #include "DebugCSPU.h"
    #define DBG_xxxx 0
    #define DBG_yyyy 1

    void setup() {
      DINIT(115200);
      (...)
    }

    //anywhere else in your code:
    DPRINTS (DBG_xxx, "My value:");
    DPRINTLN(DBG_xxx, value);
    DASSERT(value > 5);
    @endcode
 *
 *  @remark
 *  We could not find any way to detect whether Serial.begin() has previously
 *  been called or not. This library curently assumes the Serial port has been initialized,
 *  and defines macro DINIT() even when DEBUG is not defined.
 *  
 *  @par Technical note
 *  preprocessor symbol __FILE_NAME__ provides the full path and symbol __BASE_FILE__ 
 *  provides the file as passed to gcc, which happens to be the full path as well. As a consequence, 
 *  every single assertion consumes a significant amount of program memory (can be about 100-150 bytes 
 *  depending on the file path!). A detailed investigation concluded that gcc does not provide the base 
 *  name of the file and there is no portable compile-time solution to strip the path or even to limit 
 *  it to the last 20 characters.
 *  
 *  @par Dependencies
 *  None.
 *  
 *  
 *  @par History
 *  The library was created by the 2017-18 Cansat team (ISATIS)
 *  The library has been retested on board Adafruit Feather M0 Express by the 2018-19 Cansat team (IsaTwo)
 *  
 *  @{
 */

#pragma once
#include "Arduino.h"
#include "AssertCSPU.h"
#include "SerialStream.h"

#ifndef NULL 
/** @ingroup DebugCSPU.h
  @brief Definition of NULL macro to avoid inclusion of large standard header. 

  This is required to avoid including stddef.h for the NULL definition.
  Using std::nullptr is not an option because in Arduino v1.6, the required gcc option is 
  not activated to make nullptr available in namespace std.
*/
#define NULL (void *) 0
#endif


// Call DINIT once in setup() if the serial port to the computer is used in any library
// or in any part of the sketch..
// Macros do not call functions in order to reduce memory usage.
/** @ingroup DebugCSPU
 *  @brief Initialise the Serial port to the computer for use by this library or any other.
 *  @param baudRate The baud rate to use on the serial link.
 *  
 *  This macro is defined, even if symbol DEBUG is undefined.
 */
#define DINIT(baudRate)  Serial.begin(baudRate); while (!Serial); Serial.println(F("Serial link OK"));

#ifdef DEBUG
// -------- Define tracing macros -----

// Actual macros.
// Print a debugging STRING msg, if the moduleTag != 0 (no final carriage return)
// The string is not stored in dynamic memory
#define DPRINTS1(moduleTag, msg)   if(moduleTag!=0) Serial.print(F(msg));
#define DPRINTS2(moduleTag, msg,format)   if(moduleTag!=0) Serial.print(F(msg),format);

// Print a debugging STRING msg, if the moduleTag != 0 (with final carriage return)
// The string is not stored in dynamic memory
#define DPRINTSLN1(moduleTag, msg)  if(moduleTag!=0) Serial.println(F(msg));
#define DPRINTSLN2(moduleTag, msg,format)  if(moduleTag!=0) Serial.println(F(msg),format);

// Print a debugging NUMERIC value, if the moduleTag != 0 (no final carriage return)
#define DPRINT1(moduleTag, numValue)    if(moduleTag!=0) Serial.print(numValue);
#define DPRINT2(moduleTag, numValue,format)    if(moduleTag!=0) Serial.print(numValue,format);

// Print a debugging NUMERIC value, if the moduleTag != 0 (with final carriage return)
#define DPRINTLN1(moduleTag, numValue)  if(moduleTag!=0) Serial.println(numValue);
#define DPRINTLN2(moduleTag, numValue,format)  if(moduleTag!=0) Serial.println(numValue,format);

// Wait for x msec if the moduleTag != 0  is active.
#define DDELAY(moduleTag,durationInMsec)    if(moduleTag!=0) delay(durationInMsec);

// Print free ram if the moduleTag != 0
#define DFREE_RAM(moduleTag) if(moduleTag!=0)  { Serial.print(F("Free RAM: ")); Serial.print(freeRam()); Serial.println(F(" bytes."));}

// Allow for variable number of arguments (2 or 3)  (a bit tricky, but fool-proof).
// Variable number of arguments for printing string is not supported by Serial.print() but provided
// for consistency.
// Ref: https://stackoverflow.com/questions/11761703/overloading-macro-on-number-of-arguments
#define DGET_MACRO(moduleTag, _1,_2,NAME,...) NAME

#define DPRINT(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINT2, DPRINT1)(moduleTag,__VA_ARGS__)
#define DPRINTLN(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINTLN2, DPRINTLN1)(moduleTag,__VA_ARGS__)
#define DPRINTS(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINTS2, DPRINTS1)(moduleTag,__VA_ARGS__)
#define DPRINTSLN(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINTSLN2, DPRINTSLN1)(moduleTag,__VA_ARGS__)

// -------- Function prototypes
int freeRam();  // Return free dynamic memory in bytes.
#else
// Define all macros but DINIT() as blank lines.
/** @ingroup DebugCSPU
 *  @brief Print a string on the Serial output, without a final carriage-return, provided the debugging is activated.
 *  The macro takes care of running the F() macro on msg to store it in program memory. 
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect. 
 *  @param msg The string to print.
 *  @pre The serial interface has been initialised (using DINIT() macro). 
 *  
 *  if DEBUG is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DPRINTS(moduleTag, msg,...)
/** @ingroup DebugCSPU
 *  @brief Print a string on the Serial output, with a final carriage-return, provided the debugging is activated.
 *  The macro takes care of running the F() macro on msg to store it in program memory. 
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect. 
 *  @param msg The string to print.
 *  @pre The serial interface has been initialised (using DINIT() macro). 
 *  
 *  if DEBUG is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DPRINTSLN(moduleTag, msg,...)
/** @ingroup DebugCSPU
 *  @brief Print a numerical value on the Serial output, without a final carriage-return, provided the debugging is activated.
 *  @pre The serial interface has been initialised (using DINIT() macro). 
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect.
 *  @param numValue A numerical value (integer or float). 
 *  
 *  if DEBUG is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DPRINT(moduleTag, numValue,...)
/** @ingroup DebugCSPU
 *  @brief Print a numerical value on the Serial output, with a final carriage-return, provided the debugging is activated.
 *  @pre The serial interface has been initialised (using DINIT() macro). 
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect.
 *  @param numValue A numerical value (integer or float). 
 *  
 *  if DEBUG is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DPRINTLN(moduleTag, numValue,...)
/** @ingroup DebugCSPU
 *  @brief Pause program execution for some time, provided the debugging is activated.
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect.
 *  @param durationInMsec The duration of the requested pause.
 *  
 *  if DEBUG is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DDELAY(moduleTag,durationInMsec,...)
/** @ingroup DebugCSPU
 *  @brief Print the amount of free dynamic memory on the serial interface, provided the debugging is activated.
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect.
 *  
 *  if DEBUG is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DFREE_RAM(moduleTag)
#endif

/** @} */
