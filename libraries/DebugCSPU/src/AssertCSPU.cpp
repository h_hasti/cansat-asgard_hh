
#include "AssertCSPU.h"
#include "Arduino.h"

/** @ingroup DebugCSPU.h 
 *  @brief Handle diagnostic informations given by assertion and abort program execution
 */
void failCSPU(const __FlashStringHelper* __file, int __lineno, const __FlashStringHelper* __sexp) {
  // transmit diagnostic informations through serial link.
  Serial.println(F("** Assertion KO"));
  Serial.print(__file);
  Serial.print(F(":"));
  Serial.println(__lineno, DEC);
  Serial.println(__sexp);
  Serial.flush();
  // abort program execution.
  abort();
}
