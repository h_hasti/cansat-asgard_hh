/*
 * SerialStream.h
 * 
 * A couple of template function allowing to use streaming operators with Arduino's Serial objects 
 */
/** 
 *  @ingroup DebugCSPU 
 *  @{
 */
#pragma once
#include "Arduino.h"

/**@brief  Streaming operator for Arduino's Print objects (e.g. Serial).
 * 
 * This template for operator<< allows for constructs like
 * @code
 * Serial << F("GPS unit #") << gpsno << F(" reports lat/long of ") << lat << "/" << long << ENDL; 
 * @endcode
 * Source: https://playground.arduino.cc/Main/StreamingOutput 
 * @todo No support for HEX and DEC in streaming yet.
 * @remarks
 * 1/2/2018: No easy support found for HEX and DEC: defining a manipulator only works if the
 *           stream reacts on its return type to change its internal state. Since we cannot
 *           change the Print classe behaviour, only operators can be used.
 *           operator <<= allows streaming integers in hex format, but precedence rules do not
 *           allow to combine  with << as in Serial <<= anInt << " and another in " << anOtherInt;
 *           No obvious support for selection number of decimals in float either.
 */
template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }

/* @cond HIDE_FROM_DOXYGEN 
 * Convenience macro to allows constructs like
 * @code Serial vv("GPS unit #") << gpsno << ENDL; @endcode
 * in order to hide the F macro. Not used in Cansat 2018-19, not very readable: do not use!
 */
#define vv(str)  << F(str)
/* Convenience macro to allows constructs like
 * @code Serial VV("GPS unit #") << gpsno << ENDL; @endcode
 * in order to hide the F macro. Not used in Cansat 2018-19, not very readable: do not use!
 */
#define VV(str)  << F(str)
/** @endcond */

/** End-of-line macro, since std::endl cannot be used. Allows constructs like
 * @code Serial << "message followed by end-of-line" << ENDL; @endcode
 */
#define ENDL "\n"

/** @} */

#ifdef NEVER
// This was an attempt at using another operator for hexadecimal display. It does not work
// because of the precedence rules and modifies the numbers...
template<class T> inline Print &operator <<=(Print &obj, T arg) { obj.print(arg, HEX); return obj; }

// This was an attempt at stream manipulator that cannot possibly be used since we cannot
// alter the Print class.
struct SetFormat_t { int format; };
/*
 *  @brief  Manipulator for Print streams.
 *  @param  c  The new fill character.
 *
 *  Sent to a stream object, this manipulator calls print(arg, format) for that
 *  object.
 */
template<class T>
inline SetFormat_t  format(T arg, int theFormat)
     {
       SetFormat_t __x;
       __x.format=theFormat;
      return __x;
 }
#endif

