/*
   IsatisAcquisitionProcess.h
   A class derived from AcquisitionProcess, which implements the ISATIS-specific
    logic.

    Created on: 20 janv. 2018

*/

#pragma once

#include "AcquisitionProcess.h"
#include "IsatisConfig.h"
#include "IsatisStorageManager.h"
#include "IsatisHW_Scanner.h"
#include "Adafruit_BMP280.h"
#include "IsatisDataRecord.h"

class IsatisAcquisitionProcess: public AcquisitionProcess {
  public:

    IsatisAcquisitionProcess();

    /* Call once and only once before calling run() */
    virtual void init();

    virtual HardwareScanner* getHardwareScanner();
  protected:
    virtual void storeDataRecord(const bool campaignStarted);
    virtual void doIdle();

  private:
    virtual void acquireDataRecord();
    virtual void initSpecificProject();

    void discardIncomingRF_Data() const;


    bool campaignStarted ;
    float referenceAltitude;
    virtual bool measurementCampaignStarted();


    IsatisHW_Scanner hwScanner;
    IsatisDataRecord buffer;
    IsatisStorageManager storageMgr;
    Adafruit_BMP280 bmp;  // Open issue: could we create an instance of bmp whenever needed, 
                          // in order to save some dynamic memory ?
};

