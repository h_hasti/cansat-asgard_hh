/*
   IMU_Client.cpp
*/

#include "IMU_Client.h"

IMU_Client::IMU_Client(): lsm() {}

bool IMU_Client::begin() {
  if (!lsm.begin()) return false;
 
  // configure lsm
  // 1.) Set the accelerometer range
  lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_4G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_6G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_8G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_16G);

  // 2.) Set the magnetometer sensitivity
  lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_4GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_8GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_12GAUSS);

  // 3.) Setup the gyroscope
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_500DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_2000DPS);
}

void IMU_Client::readData(IsaTwoRecord& record) {

  sensors_event_t accel, mag, gyro, temp;
  lsm.getEvent(&accel, &mag, &gyro, &temp);

  record.mag[0] = mag.magnetic.x * 100;
  record.mag[1] = mag.magnetic.y * 100;
  record.mag[2] = mag.magnetic.z * 100;

  record.accel[0] = accel.acceleration.x;
  record.accel[1] = accel.acceleration.y;
  record.accel[2] = accel.acceleration.z;

  record.accelRaw[0] = lsm.accelData.x;
  record.accelRaw[1] = lsm.accelData.y;
  record.accelRaw[2] = lsm.accelData.z;

  record.gyroRaw[0] = lsm.gyroData.x;
  record.gyroRaw[1] = lsm.gyroData.y;
  record.gyroRaw[2] = lsm.gyroData.z;

  record.gyro[0] = gyro.gyro.x;
  record.gyro[1] = gyro.gyro.y;
  record.gyro[2] = gyro.gyro.z;

}
