#include "Arduino.h"
#include "IsaTwoRecord.h"
#include "IsaTwoInterface.h"

#define DEBUG
#include <DebugCSPU.h>
#define DBG 1

static constexpr char separator = ',';

IsaTwoRecord::IsaTwoRecord () {
  IsaTwoRecord::clear();
}

void IsaTwoRecord::clear() {
  timestamp = 0;
  //A. IMU DATA
  clearArray (accelRaw);
  clearArray (accel);
  clearArray (gyroRaw);
  clearArray (gyro);
  clearArray (mag);

  //B. GPS DATA
  newGPS_Measures = 0.0;
  GPS_LatitudeDegrees = 0.0;
  GPS_LongitudeDegrees = 0.0;
  GPS_Altitude = 0.0;
  GPS_VelocityKnots = 0.0;
  GPS_VelocityAngleDegrees = 0.0;

#ifdef INCLUDE_AHRS_DATA
  //C. AHRS DATA
  clearArray(AHRS_Accel);
  roll = 0.0;
  yaw = 0.0;
  pitch = 0.0;
#endif

  temperature = 0.0;
  pressure = 0.0;
  altitude = 0.0;

  //D. ADD HERE SECONDARY MISSION DATA AND VARIABLES ONCE AVAILABLE
  data1 = 0;
  data2 = 0;

}
void IsaTwoRecord::clearArray(float arr[3]) {
  for (int i = 0; i < 3; i++) {
    arr[i] = 0;
  }
}
void IsaTwoRecord::clearArray(int16_t arr[3]) {
  for (int i = 0; i < 3; i++) {
    arr[i] = 0;
  }
}
void IsaTwoRecord::printCSV(Stream& str, const float arr[3], bool finalSeparator) const {
  str.print(arr[0], numDecimalPositionsToUse); str << separator;
  str.print(arr[1], numDecimalPositionsToUse); str << separator;
  str.print(arr[2], numDecimalPositionsToUse);
  if (finalSeparator) {
    str << separator;
  }
}
void IsaTwoRecord::printCSV(Stream& str, const int16_t arr[3], bool finalSeparator) const {
  str << arr[0] << separator << arr[1] << separator << arr[2];
  if (finalSeparator) {
    str << separator;
  }
}

void IsaTwoRecord::printCSV(Stream& str, const float &f, bool finalSeparator) const {
  str.print(f, numDecimalPositionsToUse);
  if (finalSeparator) {
    str << separator;
  }
}

void IsaTwoRecord::printCSV_Header(Stream& str, DataSelector select) const {
  str << F( "type, timestamp, ");
  if (select == DataSelector::All) {
    printCSV_HeaderPart(str, DataSelector::IMU, true);
    printCSV_HeaderPart(str, DataSelector::GPS_AHRS, true);
    printCSV_HeaderPart(str, DataSelector::PrimarySecondary, false);
  }
  else {
    printCSV_HeaderPart(str, select, false);
  }
}

void IsaTwoRecord::printCSV_HeaderPart(Stream& str, DataSelector select, bool finalSeparator) const {
  switch (select) {
    case DataSelector::IMU:
      str << F("accelRawX,accelRawY,accelRawZ,accelX,accelY,accelZ,");
      str << F("gyroRawX,gyroRawY,gyroRawZ,gyroX,gyroY,gyroXZ,");
      str << F("magX,magY,magZ");
      break;
    case DataSelector::GPS_AHRS :
      str << F("GPS_Measures,GPS_LatitudeDegrees,");
      str << F("GPS_LongitudeDegrees,GPS_Altitude,GPS_VelocityKnots,GPS_VelocityAngleDegrees");
#ifdef INCLUDE_AHRS_DATA
      str << F(",AHRS_AccelX,AHRS_AccelY,AHRS_AccelZ,roll,yaw,pitch");
#endif
      break;
    case DataSelector::PrimarySecondary :
      str << F("temperature,pressure,altitude,");
      str << F("data1,data2");
      break;
    default:
      DPRINT(DBG, "Unexpected DataSelector value. Terminating Program");
      DASSERT (false);
  }
  if (finalSeparator) {
    str << separator;
  }
}

void IsaTwoRecord::printCSV_Part(Stream& str, DataSelector select, bool finalSeparator) const {
  switch (select) {
    case DataSelector::IMU :
      printCSV(str, accelRaw, true);
      printCSV(str, accel, true);
      printCSV(str, gyroRaw, true);
      printCSV(str, gyro, true);
      printCSV(str, mag);
      if (finalSeparator == true) {
        str << separator;
      }
      break;

    case DataSelector::GPS_AHRS :
      printCSV(str, newGPS_Measures, true);
      printCSV(str, GPS_LatitudeDegrees, true);
      printCSV(str, GPS_LongitudeDegrees, true);
      printCSV(str, GPS_Altitude, true);
      printCSV(str, GPS_VelocityKnots, true);
      printCSV(str, GPS_VelocityAngleDegrees, false);
#ifdef INCLUDE_AHRS_DATA
      str << separator;
      printCSV(str, AHRS_Accel, true);
      printCSV(str, roll, true);
      printCSV(str, yaw, true);
      printCSV(str, pitch, false);
#endif
      if (finalSeparator) {
        str << separator;
      }
      break;

    case DataSelector::PrimarySecondary :
      printCSV(str, temperature, true);
      printCSV(str, pressure, true);
      printCSV(str, altitude, true);
      printCSV(str, data1, true);
      printCSV(str, data2, false);
      if (finalSeparator) {
        str << separator;
      }
      ///TODO, WHEN SECONDARY MISSION DATA WILL BE THERE, ADD THEM TO THE STREAM
      break;

    default:
      DPRINT(DBG, "Unexpected DataSelector value. Terminating Program");
      DASSERT (false);
  }
}
void IsaTwoRecord::printCSV(Stream& str, DataSelector select) const {
  str <<  (uint16_t) IsaTwoRecordType::DataRecord << separator << timestamp << separator;
  if (select == DataSelector::All) {
    printCSV_Part(str, DataSelector::IMU, true);
    printCSV_Part(str, DataSelector::GPS_AHRS, true);
    printCSV_Part(str, DataSelector::PrimarySecondary, false);
  }
  else {
    printCSV_Part(str, select, false);
  }
}
