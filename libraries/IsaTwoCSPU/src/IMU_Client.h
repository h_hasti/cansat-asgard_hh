/*
 * IMU_Client.h
 */
 
#pragma once

#include "Arduino.h"
#include "Wire.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_LSM9DS0.h"
#include "IsaTwoRecord.h"

/** A class to query the LSM9DS0 Inertial Management Unit and 
 *  store the results into an IsaTwoRecord for broadcasting.
 */
class IMU_Client
{
  public:
    /** Constructor. Assumes LSM9DS0 is connected to the SDA and SCL pins 
     */
    IMU_Client();
    /** Initialize driver. Call before using readData()
     *  @return true if initialization ok.
     *  @pre Serial and Wire must be initialized
     */
    bool begin();
    /** Make a reading all 9 sensors, and store magnetic field in µT, gyro
     *  in raw sensor steps and rps, acceleration in raw sensor steps and m/s^2.
     *  NB: the record timestamp is NOT modified.
     *  @param record The record to fill with the data.
     */
    void readData(IsaTwoRecord& record);
    //void lsm(byte SDA, byte SCL);
  protected:
    Adafruit_LSM9DS0 lsm ;
};
