/* 
 *  BMP280_Client.h
 */
#pragma once

#include "IsaTwoRecord.h"
#include "Adafruit_BMP280.h"
#include "Adafruit_Sensor.h"
#include "Wire.h"
/** @ingroup IsaTwoCSPU
    xxxxxxx
*/
class BMP280_Client {
  public:
  BMP280_Client();
    bool begin(float theSeaLevelPressure);
    /** @brief Read sensor data and populate data record.
     *  @param record The data record to populate (fields temparature, altitude and pressure).
     *  @return True if reading was successful.
     */
   
    bool readData(IsaTwoRecord& record);
    

  protected:
  Adafruit_BMP280 bmp;
  float seaLevelPressure; // in hPA
  
};
