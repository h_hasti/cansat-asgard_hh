#pragma once

#include "Arduino.h"
#include "IsaTwoConfig.h"

//define INCLUDE_AHRS_DATA   // Define to transport AHRS output data. 
/** @ingroup IsaTwoCSPU
    The record carrying all data acquired or computed by the CanSat, except for images.
    Its record type must be IsaTwoRecordType::DataRecord (cf. IsaTwoInterface.h) to distinguish from commands.
*/

class IsaTwoRecord  {
  public:

    IsaTwoRecord ();

    /** Enum values, allowing for restricting the CSV output to a subset of the record */
    enum class DataSelector {
      All,         /**< Select complete record */
      IMU,         /**< Select data from the IMU only (raw & calibrated) */
      GPS_AHRS,    /**< Select data from the GPS and AHRS modules only */
      PrimarySecondary /**< Select data from primary and secondary sensors only */
    } ;

    /** Stream the record in human-readable format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                      complete records, if the data must be parsed by code expecting a IsaTwoRecord).
    */
    void print(Stream& str, DataSelector select = DataSelector::All) const;
    /** Stream the record in CSV format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                      complete records, if the data must be parsed by code expecting a IsaTwoRecord).
    */
    void printCSV(Stream& str, DataSelector select = DataSelector::All) const;
    /** Stream the header line in CSV format into str
        @param str The destination stream
        @param select A data selector to restrict the data to a subset of the record (for testing only: always output
                      complete records, if the data must be parsed by code expecting a IsaTwoRecord).
    */
    void printCSV_Header(Stream& str, DataSelector select = DataSelector::All) const;

    /** Set all values to 0 or false */
    void clear();

    // Data members
    unsigned long timestamp{};  /**< Record timestamp in msec. */

    // A. IMU data
    int16_t accelRaw[3];    /**< The raw accelerometer readings */
    float   accel[3];       /**< The accelerometer reading (m/s^2) after calibration */
    int16_t gyroRaw[3];         /**< The raw gyroscope readings */
    float gyro[3];            /**< The gyroscope readings (rps) after calibration */
    float mag[3];             /**< The magnetometer readings in µT */

    // B. GPS data
    bool  newGPS_Measures;        /**< true if GPS data is included in the record */
    float GPS_LatitudeDegrees;    /**< The latitude in decimal degrees, ([-90;90], + = N, - = S) */
    float GPS_LongitudeDegrees;   /**< The longitude in decimal degrees ([-180;180], + =E, - =W) */
    float GPS_Altitude;           /**< Altitude of antenna, in meters above mean sea level (geoid) */
    float GPS_VelocityKnots;      /**< velocity over ground in Knots (1 knot = 0.5144447 m/s) */
    float GPS_VelocityAngleDegrees; /**< Direction of velocity in decimal degrees, 0 = North */

#ifdef INCLUDE_AHRS_DATA
    // C. AHRS data
    float AHRS_Accel[3];          /**< The acceleration as computed by the AHRS (inertial referential, m/s^2) */
    float roll;                 /**< The roll angle computed by the AHRS (inertial referential, rad) */
    float yaw;                  /**< The yaw angle computed by the AHRS (inertial referential, rad) */
    float pitch;                /**< The pitch angle computed by the AHRS (inertial referential, rad) */
#endif

    // D. Primary mission data
    float temperature;    /**< The temperature in °C */
    float pressure;       /**< The pressure in hPA */
    float altitude;       /**< The altitude (m) as derived from the pressure */

    // E. Secondary mission data
    float data1;
    float data2;

  protected:
    /** Utility function: print an array of 3 numbers in CSV format, using the right number of
        decimal positions, from IsaTwoConfig.h
        @param str The output stream
        @param arr The array of 3 numbers to print.
    */
    void printCSV(Stream& str, const float arr[3], bool finalSeparator = false) const;
    /** @copydoc printCSV(Stream& str, const float arr[3], bool finalSeparator = false) const */
    void printCSV(Stream& str, const int16_t arr[3], bool finalSeparator = false) const;

    /** Utility function: print a float, using the right number of decimal positions, from
        IsaTwoConfig.h, with a final comma, if finalSeparator is true.
    */
    void printCSV(Stream& str, const float &f, bool finalSeparator = false) const;
    /** Utility function: print an array of 3 numbers in human-readable format, using the right
        number of decimal positions, from IsaTwoConfig.h
        @param str The output stream
        @param arr The array of 3 numbers to print.
    */
    void print(Stream& str, const float arr[3]) const;
    /** @copydoc print(Stream& str, const float arr[3]) const */
    void print(Stream& str, const int16_t arr[3]) const;
    /** Utility function: set an array of 3 numbers to 0
        @param arr The array of 3 numbers to print.
    */
    void clearArray(float arr[3]);
    /** @copydoc clearArray(float arr[3]) */
    void clearArray(int16_t arr[3]);

    void printCSV_HeaderPart(Stream& str, DataSelector select, bool finalSeparator) const;
    void printCSV_Part(Stream & str, DataSelector select, bool finalSeparator) const;
    void print_Part(Stream & str, DataSelector select) const;

    friend class IsaTwoRecord_Test;
} ;
