/*
 * test_IMU_Client.ino
 * 
 * Test file for class IMU_Client.
 */
 
#include "DebugCSPU.h"
#include "IsaTwoRecord.h"
#include "IMU_Client.h"
#include "Wire.h"
#include "elapsedMillis.h"

IMU_Client imu;
IsaTwoRecord record;
elapsedMillis elapsed;

void setup() {
  DINIT(115200);
  Wire.begin();
  imu.begin();
  if (!imu.begin())
  {
    /* There was a problem detecting the LSM9DS0 ... check your connections */
    Serial << "Ooops, no LSM9DS0 detected ... Check your wiring!" << ENDL;
    while (1);
  } else {
    Serial << "LSM9DS0 detected, wiring OK, proceed." << ENDL;
  }
}

void loop() {
  elapsed=0;
  imu.readData(record);
  Serial << "Took a reading in " << elapsed << " msecs" << ENDL;
  record.print(Serial, record.DataSelector::IMU);
  delay(1000);
}
