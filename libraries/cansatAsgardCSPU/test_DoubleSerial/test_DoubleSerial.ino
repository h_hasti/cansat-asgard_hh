#include "DoubleSerial.h"
#define DEBUG
#include "DebugCSPU.h"

/**@ingroup cansatAsgardCSPU
   @brief This the Unit Test program for class @p DoubleSerial.
   @pre If you are using as TTL-to-USB converter the PL2303HX you must install
        the driver (the driver is on the link below) because their is an issue
        on windows devices with the other drivers.
   @pre Link: https://www.youtube.com/redirect?v=aWquKi_e-3A&event=video_description&q=http%3A%2F%2Fwp.brodzinski.net%2Fwp-content%2Fuploads%2F2014%2F10%2FIO-Cable_PL-2303_Drivers-Generic_Windows_PL2303_Prolific.zip&redir_token=yYkIZjmBRrA1kMlKNnZw5IMpMad8MTU0MTY5Mzk0OUAxNTQxNjA3NTQ5
*/

DoubleSerial myDoubleSerial(11, 10, 17);
const char* lineToGoToUSB_TTL1 = "17, azertyuiop";
const char* lineToGoToUSB_TTL2 = "17, with a final CR\n";
const char* lineToGoToUSB_TTL3 = "017      , with additional blanks";
const char* lineToGoToSerial1 = "83, azertyuiop";
const char* lineToGoToSerial2 = "not a number, azertyuiop";
const char* lineToGoToSerial3 = "a line wihout a comma";
const char* lineToGoToSerial4 = "17.1, not an integer with final CR\n";
const char* lineToGoToSerial5 = "17.1, not an integer";
const char* lineToGoToSerial6 = "17 right selector, but no comma after number";

void performTest() {
  myDoubleSerial << lineToGoToUSB_TTL1 << lineToGoToUSB_TTL2;
  myDoubleSerial << lineToGoToUSB_TTL3;
  myDoubleSerial << lineToGoToSerial1 << lineToGoToSerial2;
  myDoubleSerial << lineToGoToSerial3 << lineToGoToSerial4;
  myDoubleSerial << lineToGoToSerial5 << lineToGoToSerial6;


  Serial << ENDL << ENDL
         << "You should have received without any blank or new-lines "
         << "between the strings except when they are 'with final CR':" << ENDL
         << "A. On this Serial port:" << ENDL
         << "    " << lineToGoToSerial1 << ENDL
         << "    " << lineToGoToSerial2 << ENDL
         << "    " << lineToGoToSerial3 << ENDL
         << "    " << lineToGoToSerial4 << ENDL
         << "    " << lineToGoToSerial5 << ENDL
         << "    " << lineToGoToSerial6 << ENDL
         << "B. On the second serial port:" << ENDL
         << "    " << lineToGoToUSB_TTL1 << ENDL
         << "    " << lineToGoToUSB_TTL2 << ENDL
         << "    " << lineToGoToUSB_TTL3 << ENDL
         << "If it is so, everything is OK" << ENDL;
  myDoubleSerial.flush();
}
void setup() {
  DINIT(115200);
  myDoubleSerial.begin(115200);
  performTest();
}

void loop() {
}
