/*
   AcquisitionProcess.cpp

   Created on: 19 janv. 2018
*/


#include "AcquisitionProcess.h"
#include "Arduino.h"
#define DEBUG
//#define USE_ASSERTION
//#define USE_TIMER
#include "DebugCSPU.h"
#include "Timer.h"
#define DBG_RUN 0

AcquisitionProcess::AcquisitionProcess(
  unsigned int acquisitionPeriodInMsec) {
  periodInMsec = acquisitionPeriodInMsec;
  elapsedTime = acquisitionPeriodInMsec + 1; // Ensure next run will trigger an acquisition cycle.
  heartbeatLED_State = Off;
}

void AcquisitionProcess::blinkHeartbeatLED() {


  if ( (heartbeatTimer >= 1000) && (heartbeatLED_State == Off) ) {
    setLED(HardwareScanner::Heartbeat, On);
    heartbeatTimer = 0 ;
    heartbeatLED_State = On;
  }

  if ((heartbeatTimer >= 1000) && (heartbeatLED_State == On) ) {
    setLED(HardwareScanner::Heartbeat, Off);
    heartbeatTimer = 0 ;
    heartbeatLED_State = Off;
  }
}

void AcquisitionProcess::run() {
  blinkHeartbeatLED();

  DASSERT(storageMgr != nullptr);
  if (elapsedTime > periodInMsec) {
    DBG_TIMER("AcqProc::runReal");
    elapsedTime = 0;    // Reset timer here, to avoid adding processing time to the cycle duration.
    DPRINTSLN(DBG_RUN, "Running Process");
    setLED(HardwareScanner::Acquisition, On);
    acquireDataRecord();
    setLED(HardwareScanner::Acquisition, Off);

    if (isRecordRelevant()) {
      setLED(HardwareScanner::Storage, On);
      storeDataRecord(measurementCampaignStarted());
      setLED(HardwareScanner::Storage, Off);
    }
  }
  doIdle();
}

void AcquisitionProcess::initLED_Hardware()  {
  HardwareScanner *hw = getHardwareScanner();
  pinMode(hw->getLED_PinNbr(HardwareScanner::Init), OUTPUT);
  pinMode(hw->getLED_PinNbr(HardwareScanner::Storage), OUTPUT);
  pinMode(hw->getLED_PinNbr(HardwareScanner::Transmission), OUTPUT);
  pinMode(hw->getLED_PinNbr(HardwareScanner::Acquisition), OUTPUT);
  pinMode(hw->getLED_PinNbr(HardwareScanner::Heartbeat), OUTPUT);
  pinMode(hw->getLED_PinNbr(HardwareScanner::Campaign), OUTPUT);
  pinMode(hw->getLED_PinNbr(HardwareScanner::UsingEEPROM), OUTPUT);
  delay(500);  // All LEDs on for 500 ms.
  setLED(HardwareScanner::Init, Off);
  setLED(HardwareScanner::Storage, Off);
  setLED(HardwareScanner::Transmission, Off);
  setLED(HardwareScanner::Acquisition, Off);
  setLED(HardwareScanner::Heartbeat, Off);
  setLED(HardwareScanner::Campaign, Off);
  setLED(HardwareScanner::UsingEEPROM, Off);
}

void  AcquisitionProcess::init() {
  initLED_Hardware();
  setLED(HardwareScanner::Init, On);
  setLED(HardwareScanner::Campaign, On); // This LED remains On Until campaign is started.
  initSpecificProject();
  setLED(HardwareScanner::Init, Off);
}

void AcquisitionProcess::setLED(HardwareScanner::LED_Type type, bool status) {
  int pinNumber = getHardwareScanner()->getLED_PinNbr(type);
  digitalWrite(pinNumber, status ? LOW : HIGH);
  // write LOW sets LED ON.
}

bool AcquisitionProcess::isRecordRelevant() const {
  return true;
}

