//SD_Logger 19/02/2018

#include "SD_Logger.h"

//#define USE_ASSERTIONS
//#define USE_TIMER

#define DEBUG
#include "DebugCSPU.h"
#include "Timer.h"

#define DBG_INIT_SD 0
#define DBG_LOG_SD 0
#define DBG_BUILD_FILE_NAME_SD 0
#define DBG_FILENAME_SD 0
#define DBG_FREE_SPACE 0
#define DBG_DIAGNOSTIC 1

SD_Logger::SD_Logger() {
  logFileName.reserve(13); // To avoid heap fragmentation.
  logFileName = "noname.txt";
}

char SD_Logger::init( const char * fourCharPrefix,
                      const byte chipSelectPinNumber,
                      const String& msg,
                      const unsigned int requiredFreeMegs) {
  DPRINTSLN(DBG_INIT_SD, "Initializing SD card...");
  DBG_TIMER("SD_Logger::init");
#ifndef STORE_SDFAT_OBJECT
  SdFat SD;
  this->chipSelectPinNumber=chipSelectPinNumber;
#endif

  if (!SD.begin(chipSelectPinNumber)) {
    DPRINTS(DBG_INIT_SD, "Card failed, or not present");
    return -1;
    // do nothing more
  }
  DPRINTSLN(DBG_INIT_SD, "SD Init OK");

  if (requiredFreeMegs > freeSpaceInMBytes(SD) ) {
    DPRINTS(DBG_INIT_SD, "Not enough space.");
    return 1;
  }

  unsigned int counter = 1;

  do {
    buildFileName(fourCharPrefix, counter);
    counter++;
  } while ( SD.exists(logFileName.c_str()));

  DPRINTS(DBG_INIT_SD, "Name of the file: ");
  DPRINTLN(DBG_INIT_SD, logFileName);

  File dataFile = SD.open(logFileName, FILE_WRITE);
  DPRINTS(DBG_INIT_SD, "File opened.");

  if (msg.length() > 0) {

    DPRINTS(DBG_INIT_SD, "writing...");
    dataFile.println(msg);
    DPRINTSLN(DBG_INIT_SD, "written");
    DPRINTLN(DBG_INIT_SD, msg);
  }
  dataFile.close();
  DPRINTSLN(DBG_INIT_SD, "closed.");
  DPRINTSLN(DBG_INIT_SD, "card initialized.");
  return 0;
}

bool SD_Logger::log(const String& data, const bool addFinalCR) {
  DBG_TIMER("logger::log");
  bool result = false;

#ifndef STORE_SDFAT_OBJECT
  SdFat SD;
  if (!SD.begin(chipSelectPinNumber)) {
    DPRINTS(DBG_LOG_SD, "Card failed, or not present");
    return false;
    // do nothing more
  }
#endif

  File dataFile = SD.open(logFileName, FILE_WRITE);

  if (dataFile) {
    if (addFinalCR) {
      dataFile.println(data.c_str());
    }
    else {
      dataFile.print(data.c_str());
    }
    dataFile.close();

    DPRINTS(DBG_LOG_SD, "data written: ");
    DPRINTLN(DBG_LOG_SD, data);
    result = true;
  }
  else {
    DPRINTSLN(DBG_DIAGNOSTIC, "error opening file");
  }
  return result;
}

const String& SD_Logger::fileName() const {
  return logFileName;
}

bool SD_Logger::doIdle() {
  bool result = false;
  if (timeSinceMaintenance > maintenancePeriod) {
    timeSinceMaintenance = 0;
    // Actual maintenance here, if any
    result = true;
  }
  return result;
}

void SD_Logger::buildFileName(const char* fourCharPrefix, const byte number) {

  logFileName = fourCharPrefix;
  DASSERT(logFileName.length() == 4);

  char numStr[5];
  sprintf(numStr, "%04d", number);

  logFileName += numStr;
  logFileName += ".txt";

  DPRINTS(DBG_BUILD_FILE_NAME_SD, "Name of the file: ");
  DPRINTLN(DBG_BUILD_FILE_NAME_SD, logFileName);
}

unsigned long SD_Logger::fileSize() {
#ifndef STORE_SDFAT_OBJECT
  SdFat SD;
  if (!SD.begin(chipSelectPinNumber)) {
    DPRINTS(DBG_LOG_SD, "Card failed, or not present");
    return 0;
    // do nothing more
  }
#endif
  File theFile = SD.open(logFileName, FILE_READ);
  uint32_t size = theFile.size();
  theFile.close();
  return size;
}

/* Provide the initialized  SdFat object: it would be another  500 bytes on the stack
 *  to create a new one!
 */
unsigned long SD_Logger::freeSpaceInMBytes(SdFat& SD)
{
  DBG_TIMER("logger::freeSpaceInMBytes");
  DPRINTSLN(DBG_DIAGNOSTIC,"Checking free space on SD card...");
  unsigned long  freeMB = SD.vol()->freeClusterCount();
  freeMB *= SD.vol()->blocksPerCluster();
  freeMB /= 2048;
  DPRINTS(DBG_FREE_SPACE, "Free space MB: ");
  DPRINTLN(DBG_FREE_SPACE, freeMB);

  return freeMB;
}

unsigned long SD_Logger::freeSpaceInBytes()  {
#ifndef STORE_SDFAT_OBJECT
  SdFat SD;
  if (!SD.begin(chipSelectPinNumber)) {
    DPRINTS(DBG_FREE_SPACE, "Card failed, or not present");
    return false;
    // do nothing more
  }
#endif
  const unsigned long overFlowLimit = UINT_MAX / 512;
  unsigned long  freeBytes = SD.vol()->freeClusterCount();
  freeBytes *= SD.vol()->blocksPerCluster();

  if (freeBytes >= overFlowLimit) {
    freeBytes = UINT_MAX;
  } else freeBytes *= 512;

  DPRINTS(DBG_FREE_SPACE, "Free space bytes: ");
  DPRINTLN(DBG_FREE_SPACE, freeBytes);

  return freeBytes;
}






