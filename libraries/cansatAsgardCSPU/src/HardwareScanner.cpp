/*
   Hardware Scanner.cpp
*/

#include "HardwareScanner.h"
#include "Wire.h"
#include "ExtEEPROM.h"

#define DEBUG
//#define USE_ASSERTIONS
#include "DebugCSPU.h"
#define DEBUG_I2C_SCAN 0
#define DEBUG_I2C_SCAN_VERBOSE 0
#define DBG_CONSTRUCTOR 0
#define DBG_INIT 0
#define DBG_EEPROM_FLAGS 0
#define DBG_IS64KB 0
#define DBG_GET_NUM_EXT_EEPROMS 0

// Define Serial-port related symbols for Feather M0 Express board
/*  We assume the default Sercom configuration is unchanged so Serial1 is 
 *  available on Sercom0.
 *  Since Sercom1 can be easily used as Serial2 on D10-D11 (cf. Serial2.h),
 *  Serial2 is assumd to be available as well.
 */
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#include "Serial2.h"
#  ifndef HAVE_HWSERIAL0
#    define HAVE_HWSERIAL0 1
#  endif
#  ifndef HAVE_HWSERIAL1
#    define HAVE_HWSERIAL1 1
#  endif
#  ifndef HAVE_HWSERIAL2
#    define HAVE_HWSERIAL2 1
#  endif
#endif

HardwareScanner::HardwareScanner(const byte unusedAnalogInPin) {
  DPRINTS(DBG_CONSTRUCTOR, "HardwareScanner::HardwareScanner, last EEPROM address=0x");
  DPRINTLN(DBG_CONSTRUCTOR, I2C_EEPROM_LastAddress, HEX);
  this->nbrOfI2C_Slaves = 0;
  this->RF_Serial = NULL;
  this->I2C_EEPROM_LastAddress = 0xFFFF; // by default any EEPROM is 64 kbytes
  this->I2C_EEPROM_Flags = 0;
  randomSeed(analogRead(unusedAnalogInPin));
  DPRINTSLN(DBG_CONSTRUCTOR, "End of constructor.");
}

void HardwareScanner::init(
  const byte firstI2C_Address,
  const byte lastI2C_Address,
  const byte RF_SerialPort,
  const uint16_t bitRateInKhz) {
  DASSERT(RF_SerialPort <= 3);
  scanI2C_Bus(firstI2C_Address, lastI2C_Address, bitRateInKhz);
  DPRINTSLN(DBG_INIT, "Checking RF...");
  if (RF_SerialPort != 0) {
    if (isSerialPortAvailable(RF_SerialPort)) {
      switch (RF_SerialPort) {
        case 1:
#if defined(UBRR1H) || defined (HAVE_HWSERIAL1)
          this->RF_Serial = &Serial1;
#endif
          break;
        case 2:
#if defined(UBRR2H) || defined (HAVE_HWSERIAL2)
          this->RF_Serial = &Serial2;
#endif
          break;
        case 3:
#if defined(UBRR3H)|| defined (HAVE_HWSERIAL3)
          this->RF_Serial = &Serial3;
#endif
          break;
        default:
          DASSERT(false);
      } // switch
    } // if RF_SerialPort available
  } //if RF_SerialPort != 0
  DPRINTSLN(DBG_INIT, "Checking SPI devices...");
  checkAllSPI_Devices();
  DPRINTSLN(DBG_INIT, "End of constructor.");
}

HardwareSerial* HardwareScanner::getRF_SerialObject() const {
  return (this->RF_Serial);
}

bool HardwareScanner::check64Kbyte(const byte I2C_Address) const {

  // Select an address in the first 32k
  EEPROM_Address address32k = random(0, (1024L * 32L) - 1);
  // Define same address in the next 32k
  EEPROM_Address address64k = address32k + 0x8000;
  DPRINTS(DBG_IS64KB, "address32k=0x");
  DPRINT(DBG_IS64KB, address32k, HEX);
  DPRINTS(DBG_IS64KB, ", address64k=0x");
  DPRINTLN(DBG_IS64KB, address64k, HEX);

  byte aByte = 0, savedByte32k = 0, savedByte64k;
  bool is64k = false;

  bool result = ExtEEPROM::readByte(I2C_Address, address32k, savedByte32k);
  DASSERT(result);
  if (!result) {
    DPRINTSLN(DBG_IS64KB,"Error reading byte");
    return false;
  }
  result = ExtEEPROM::readByte(I2C_Address, address64k, savedByte64k);
  DASSERT(result);
  if (!result) {
    DPRINTSLN(DBG_IS64KB,"Error reading byte");
    return false;
  }

  DPRINTS(DBG_IS64KB, "savedByte32k: ");
  DPRINTLN(DBG_IS64KB, savedByte32k);
  DPRINTS(DBG_IS64KB, "savedByte64k: ");
  DPRINTLN(DBG_IS64KB, savedByte64k);

  if (savedByte32k != savedByte64k) {
    is64k = true;
  }
  else {
    // Overwrite one, check it affects the other one
    aByte = savedByte32k + 1;
    while (aByte == savedByte64k) aByte++;
    // Now aByte is different from both savedByte32k and savedByte64k
    DPRINTS(DBG_IS64KB, "aByte: ");
    DPRINTLN(DBG_IS64KB, aByte);

    result = ExtEEPROM::writeByte(I2C_Address, address32k, aByte);
    DASSERT(result);
    result = ExtEEPROM::readByte(I2C_Address, address64k, savedByte64k);
    DASSERT(result);

    DPRINTS(DBG_IS64KB, "Read in second half: ");
    DPRINTLN(DBG_IS64KB, savedByte64k);

    if (savedByte64k != aByte) is64k = true;
    // Restore original data
    result = ExtEEPROM::writeByte(I2C_Address, address32k, savedByte32k);
    DASSERT(result);
  } // else
  return is64k;
}

void HardwareScanner::scanI2C_Bus(const byte firstAddress, const byte lastAddress,
                                  const byte bitRateInKhz) {
  DPRINTS(DEBUG_I2C_SCAN, "Scanning I2C bus from ");
  DPRINT(DEBUG_I2C_SCAN, firstAddress);
  DPRINTS(DEBUG_I2C_SCAN, " to ");
  DPRINTLN(DEBUG_I2C_SCAN, lastAddress);
  DASSERT(lastAddress <= 127)
  nbrOfI2C_Slaves = 0;

  Wire.begin();
  Wire.setClock(bitRateInKhz * 1000L);
  for (byte address = firstAddress; address < lastAddress; address++)
  {
    Wire.beginTransmission(address);
    if (Wire.endTransmission() == 0)
    {
      DPRINTS(DEBUG_I2C_SCAN, " ");
      DPRINT(DEBUG_I2C_SCAN, address, DEC);
      DPRINTS(DEBUG_I2C_SCAN, " (0x");
      DPRINT(DEBUG_I2C_SCAN, address, HEX);
      DPRINTS(DEBUG_I2C_SCAN, ")");
      if ((nbrOfI2C_Slaves % 5) < 4 ) {
        DPRINTS(DEBUG_I2C_SCAN, ",");
      }
      DASSERT(nbrOfI2C_Slaves < maxNbrOfI2C_Slaves);
      I2C_Adresses[nbrOfI2C_Slaves] = address;
      nbrOfI2C_Slaves++;
      // can we identify the slave?
      if ((address >= 0x50) && (address <= 0x57)) {
        byte mask = 1 << (address - 0x50);
        I2C_EEPROM_Flags |= mask;
        DPRINTS(DEBUG_I2C_SCAN, " EEPROM detected. Flags=");
        DPRINT(DEBUG_I2C_SCAN, I2C_EEPROM_Flags, BIN);
        // If it is not a 64kbyte chip, consider all chips are 32k
        if (!check64Kbyte(address)) {
          I2C_EEPROM_LastAddress = 0x7FFF;
          DPRINTSLN(DEBUG_I2C_SCAN, " Size=64k");
        }
        else   {
          DPRINTSLN(DEBUG_I2C_SCAN, " Size=32k");
        }
      }
    } // end of good response
    else {
      DPRINTS(DEBUG_I2C_SCAN_VERBOSE, " ");
      DPRINT(DEBUG_I2C_SCAN_VERBOSE, address, DEC);
      DPRINTS(DEBUG_I2C_SCAN_VERBOSE, " (0x");
      DPRINT(DEBUG_I2C_SCAN_VERBOSE, address, HEX);
      DPRINTSLN(DEBUG_I2C_SCAN_VERBOSE, ") : Dead");
    }
    delay (5);  // give devices time to recover
  } // end of for loop

  DPRINTS(DEBUG_I2C_SCAN, " Total: ");
  DPRINT(DEBUG_I2C_SCAN, nbrOfI2C_Slaves, DEC);
  DPRINTSLN(DEBUG_I2C_SCAN, " device(s).");
}

void HardwareScanner::printFullDiagnostic(Stream& stream) const {
  stream.print(F("Internal EEPROM: "));
  if (getInternalEEPROM_LastAddress()) {
    stream.print(((unsigned long) getInternalEEPROM_LastAddress()) + 1);
    stream.println(F(" bytes."));
  } else {
    stream.println("--");
  }
  delay(100);   // Delays required to avoid buffer overflow if the serial stream is slow.
  printSerialPortsDiagnostic(stream);
  delay(200);
  printI2C_Diagnostic(stream);
  delay(200);
  printSPI_Diagnostic(stream);
  delay(200);

  stream.print(F("External EEPROM: "));
  stream.println(getNumExternalEEPROM());
  for (int i = 0; i < getNumExternalEEPROM(); i++) {
    byte address = getExternalEEPROM_I2C_Address(i);
    stream.print(F("  At I2C Ox"));
    stream.print(address, HEX);
    stream.print(F(": "));
    delay(100);
    if (isI2C_SlaveUpAndRunning(address)) {
      if (check64Kbyte(address)) {
        stream.println(F("64k, OK"));
      }
      else {
        stream.println(F("32k, OK"));
      }
      delay(100);
    } else stream.println(F("**** ERROR ***"));
  } // for
  if (getNumExternalEEPROM() > 0) {
    stream.print(F("  Size used: "));
    stream.print( (int) (((long) I2C_EEPROM_LastAddress + 1) / 1024), DEC);
    stream.println(F("k"));
    delay(200);
  }
}

void HardwareScanner::printI2C_Diagnostic(Stream& stream) const {
  stream.print(F("I2C Bus: "));
  if (nbrOfI2C_Slaves == 0) stream.println(F("//"));
  else {
    stream.print(nbrOfI2C_Slaves);
    stream.print(F(" slave(s) at: "));
    for (int i = 0; i < nbrOfI2C_Slaves; i++)
    {
      if (i > 0) stream.print(F(", "));
      stream.print(I2C_Adresses[i], DEC);
      stream.print(F(" (0x"));
      stream.print(I2C_Adresses[i], HEX);
      stream.print(F(")"));
    }
    stream.println(F("."));
  }
}

void HardwareScanner::printSerialPortsDiagnostic(Stream& stream) const {
  stream.print(F("Serial ports:"));
  for (byte i = 0; i < 10; i++) {
    if (isSerialPortAvailable(i)) {
      stream.print(F(" Serial"));
      if (i > 0) stream.print(i);
    }
  }
  if (RF_Serial != NULL)
  {
    stream.println(F(".   RF Transmitter available"));
  }
  else stream.println(F(""));
}

void HardwareScanner::printSPI_Diagnostic(Stream& stream) const {
  stream.println(F("SPI bus: N/A"));
  delay(100);
  return;
}

byte HardwareScanner::getExternalEEPROM_I2C_Address(const byte EEPROM_Number) const  {
  // See note in header file.
  byte flags = I2C_EEPROM_Flags;
  int num = -1;
  byte count = 0;
  byte address = 0;

  DPRINTS(DBG_EEPROM_FLAGS, "flags=");
  DPRINT(DBG_EEPROM_FLAGS, flags, BIN);
  DPRINTS(DBG_EEPROM_FLAGS, ", looking for EEPROM #");
  DPRINTLN(DBG_EEPROM_FLAGS, EEPROM_Number);

  while ((count < 8) && (num < EEPROM_Number)) {
    num += flags % 2;
    flags >>= 1;
    count++;
    DPRINTS(DBG_EEPROM_FLAGS, "flags=");
    DPRINT(DBG_EEPROM_FLAGS, flags, BIN);
    DPRINTS(DBG_EEPROM_FLAGS, ", num=");
    DPRINTLN(DBG_EEPROM_FLAGS, num);
  }
  address = 0x50 + count - 1;

  DASSERT(num == EEPROM_Number); // Should not be called if flag not set.
  DPRINTS(DBG_EEPROM_FLAGS, "Out: num=");
  DPRINT(DBG_EEPROM_FLAGS, num);
  DPRINTS(DBG_EEPROM_FLAGS, ", count=");
  DPRINT(DBG_EEPROM_FLAGS, count);
  DPRINTS(DBG_EEPROM_FLAGS, ", address=0x");
  DPRINTLN(DBG_EEPROM_FLAGS, address, HEX);

  return address;
}

unsigned int HardwareScanner::getExternalEEPROM_LastAddress(const byte /* EEPROM_Number */) const {
  //see note in header file.
  return I2C_EEPROM_LastAddress;
}

bool HardwareScanner::isSerialPortAvailable(const byte portNbr) const {
  bool result = false;
  switch (portNbr) {
    case 0:
#if defined(UBRRH) || defined(UBRR0H) || defined (HAVE_HWSERIAL0)
      result = true;
#endif
      break;
    case 1:
#if defined(UBRR1H) || defined (HAVE_HWSERIAL1)
      result = true;
#endif
      break;
    case 2:
#if defined(UBRR2H)|| defined (HAVE_HWSERIAL2)
      result = true;
#endif
      break;
    case 3:
#if defined(UBRR3H) || defined (HAVE_HWSERIAL3)
      result = true;
#endif
      break;
    default:
      break;
  }
  return result;
}

bool HardwareScanner::isI2C_SlaveUpAndRunning(const byte slaveAddress) const {
  // We may assume that the address are sorted
  bool done = false;
  bool result = false;
  int i = 0;
  while ((i < nbrOfI2C_Slaves) && (!done)) {
    if (I2C_Adresses[i] == slaveAddress) {
      done = true;
      result = true;
    }
    if (I2C_Adresses[i] > slaveAddress) {
      done = true;
    }
    i++;
  }
  return result;
}

byte HardwareScanner::getLED_PinNbr(LED_Type /*type*/) {
  return LED_BUILTIN;
}

byte HardwareScanner::getNumExternalEEPROM() const {
  byte flags = I2C_EEPROM_Flags;
  byte num = 0;
  while (flags) {
    DPRINT(DBG_GET_NUM_EXT_EEPROMS, flags, BIN);
    num += flags % 2;
    DPRINTS(DBG_GET_NUM_EXT_EEPROMS, ", num=");
    DPRINTLN(DBG_GET_NUM_EXT_EEPROMS, num);
    flags >>= 1;
  }
  return num;
}
