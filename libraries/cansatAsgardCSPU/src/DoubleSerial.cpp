/*
   DoubleSerial.cpp
*/

// See remark in header file
#ifndef ARDUINO_SAMD_ZERO

#include "DoubleSerial.h"
#include <stdlib.h>
#define DEBUG
#include "DebugCSPU.h"
#define DBG_SP 0

DoubleSerial::DoubleSerial (
  const byte theRxPin,
  const byte theTxPin,
  const uint16_t theSelectionCode) : serial2(theRxPin, theTxPin)
{
  selectionCode = theSelectionCode;
}

void DoubleSerial::begin (const uint32_t theBaudRate) {
  serial2.begin(theBaudRate);
}

DoubleSerial& DoubleSerial::operator<<(const char * line) {
  DPRINT(DBG_SP, "Line : ");
  DPRINTLN(DBG_SP, line);
  char* nextCharAddress;
  long int code = strtol(line, &nextCharAddress, 10);
  DPRINT(DBG_SP, "Code : ");
  DPRINTLN(DBG_SP, code);
  DPRINT(DBG_SP, "Next char : ");
  DPRINTLN(DBG_SP, *nextCharAddress);
  if (code == selectionCode) {
    while (*nextCharAddress == ' ') {
      nextCharAddress++;
    }
    if (*nextCharAddress == ',') {
      serial2.println(line);
    }
    else {
      Serial.println(line);
    }
  }
  else {
    Serial.println(line);
  }
  return *this;
}

void DoubleSerial::flush() {
  Serial.flush();
  serial2.flush();
}
#endif
