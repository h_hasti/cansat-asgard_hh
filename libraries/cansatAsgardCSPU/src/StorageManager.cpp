/*
    StorageManager.cpp

    Created on: 19 janv. 2018
*/

#include "StorageManager.h"
#define DEBUG
//#define USE_ASSERTIONS
//#define USE_TIMER
#include "DebugCSPU.h"
#include "Timer.h"
#define DBG_STORE 0
#define DBG_INIT 0
#define DBG_STORAGE 0
#define DBG_DIAGNOSTIC 1

StorageManager::StorageManager(unsigned int theRecordSize,
                               unsigned int theCampainDurationInSec,
                               unsigned int theMinStoragePeriodInMsec) : eeprom(10) {
  // Do not debug or assert in constructor!
  recordSize = theRecordSize;
  campainDuration = theCampainDurationInSec;
  minStoragePeriod = theMinStoragePeriodInMsec;
  initEEPROM = false;
  initSD = false;
}

bool StorageManager::init(HardwareScanner* hwScanner,
                          const char * fourCharPrefix,
                          const byte chipSelectPinNumber,
                          const unsigned int requiredFreeMegs,
                          const EEPROM_BankWriter::EEPROM_Key key,
                          const String& loggerFirstLine) {

  DASSERT(recordSize > 0);
  DASSERT(campainDuration > 0);
  DASSERT(minStoragePeriod > 0);
  DASSERT(hwScanner != NULL);

  DPRINTSLN(DBG_INIT, "Initialising EEPROM_BankWriter...");
  bool result1 = eeprom.init(key, *hwScanner, recordSize);
  if (result1 != true) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing EEPROM_BankWriter.");
  } else {
    DPRINTSLN(DBG_INIT, "EEPROM init ok.");
    DPRINTS(DBG_DIAGNOSTIC, "Records (found/free): ");
    DPRINT(DBG_DIAGNOSTIC, (eeprom.getTotalSize() - eeprom.getFreeSpace())/recordSize);
    DPRINTS(DBG_DIAGNOSTIC, "/");
    DPRINTLN(DBG_DIAGNOSTIC, eeprom.getNumFreeRecords());
    initEEPROM = true;
  }

  int result2 = logger.init(fourCharPrefix, chipSelectPinNumber, loggerFirstLine, requiredFreeMegs);
 
  switch (result2) {
    case 0:
      DPRINTS(DBG_DIAGNOSTIC, "SD_Logger OK. Logging to ");
      DPRINTLN(DBG_DIAGNOSTIC, logger.fileName());
      initSD = true;
      break;
    case 1:
      DPRINTSLN(DBG_DIAGNOSTIC, "Requested free space not available on SD Card");
      break;
    case -1:
      DPRINTSLN(DBG_DIAGNOSTIC, "SD Logger init failed");
      break;
  }

  return (initSD || initEEPROM);
}

void StorageManager::storeOneRecord(const byte* binaryData, const String& stringData, const bool useEEPROM) {

  DPRINTSLN(DBG_STORE, "StorageManager::storeOneRecord");
  DBG_TIMER("StorageMgr::storeOnRecord");
  DASSERT(recordSize > 0);
  DASSERT(campainDuration > 0);
  DASSERT(minStoragePeriod > 0);
  DASSERT(binaryData != NULL);

  if (binaryData != NULL) {
    // We can actually store the data, but in the right format according to destination.
    // The binary version of the data is just recordSize bytes of information, that can only be stored as-is in memory.
    // The stringData is a readable version of the same information, larger but humain-readable, to store on the SD card, where
    // storage is cheap and unlimited.
    if (initSD == true) {
      DPRINTS(DBG_STORE, "Storing to SD Card...");
      logger.log(stringData);
      DPRINTSLN(DBG_STORE, "Stored to SD Card");
    }
    if ((initEEPROM == true) && useEEPROM) {
      DPRINTS(DBG_STORE, "Storing to EEPROM...");
      eeprom.storeOneRecord(binaryData, recordSize);
      DPRINTSLN(DBG_STORE, "Stored to EEPROM");
      // Do not check return codes: we can only fail silently and hope not all storage destination fails….
    }
    DPRINTSLN(DBG_STORE, "We're all proud of Lorenz");
  }
}

void StorageManager::doIdle()
{
  if (initSD) logger.doIdle();
  if (initEEPROM) eeprom.doIdle();
}

unsigned long StorageManager::getNumFreeEEEPROM_Records() const {
  if (initEEPROM == true) {
    return eeprom.getNumFreeRecords();
  }
  else return 0;
}

bool StorageManager::SD_Operational() const {
  return initSD;
}

