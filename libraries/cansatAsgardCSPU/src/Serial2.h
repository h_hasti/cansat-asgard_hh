/*
 * Serial2.h
 * 
 * This file must be included to define the Serial2 object when it is not defined in the core
 * for a particular board (If Serial2 is defined, including this file will not modify it.
 * 
 * Currently supports only boards with ATSAMD21G18 controller 
 */

#if defined(UBRR2H) || defined(HAVE_HWSERIAL2)
    // If any of the above is defined, Serial2 global object is available, 
    // Nothing need to be done.
#elif defined(__SAMD21G18A__)
#include <Arduino.h>
 
/** @ingroup cansatAsgardCSPU
 *  @brief This class encapsulates the configuration and use of Sercom module 1 to manage an additional 
 *  serial port  (Serial2) on µControllers with Sercom (Serial communication) modules. It is 
 *  developped for the ATSAMD21G18 controller used by Adafruit FeatherM0 Express board (although it 
 *  should work for any board based on the same controller.
 *  TX is on D10 and RX on D11.
 *  NB: Serial2 is a subclass of Uart, HardwareSerial and Stream. 
 *  
 *  @usage
 *  @code
 *  #include "Serial2.h"
 *  
 *  //... in your code ...
 *  Serial2.begin(115200);
 *  Serial2.println("xxxx");
 *  if (Serial2.available()) {
 *    char c = Serial2.read();
 *    // ....
 *  }
 *  @endcode
 *  Important notes:
 *   - By default, variants define (in variant.h) symbol SERIAL_PORT_HARDWARE_OPEN to identify a serial port which is open for use.  
 *     (its RX & TX pins are NOT connected to anything by default). For Adafruit Feather M0 Express, SERIAL_PORT_HARDWARE is Serial1. 
 *     They also define SERIAL_PORT_MONITOR and SERIAL_PORT_USBVIRTUAL which for Adafruit Feather M0 Express are both Serial.
 *     Those ports ARE distinct, and provide 2 serial ports out of the box. 
 *   - Some Sercoms are configured by default and should usually not be modified: 
 *     The µC ATSAMD21G18 has 5 sercom modueles: 0 is reserved 
 *     for the USB serial, 3 is used for the I2C bus, 4 for the SPI bus,  
 *     5 for Programming/Debug port (if any, not on FeatherM0). 
 *   - Not all sercoms can be configured on all pins, due to limitations in the 
 *     multiplexers (or in the configuration files). This class supports the use
 *     of Sercom 1, with TX on D10, RX on D11. 
 *   - A similar class exists for Sercom 2, with TX on D4, RX on D3
 *  Reference: @link https://learn.adafruit.com/using-atsamd21-sercom-to-add-more-spi-i2c-serial-ports/creating-a-new-serial
 *  
 */
class SercomSerial1 : public Uart {
 public:
    SercomSerial1();
    /** Initialize serial port */
    void begin(unsigned long baudRate);
    /** Obtain the TX pin number */
    byte getTX() { return tx;};
    /** Obtain the RX pin number */
    byte getRX() { return rx;};
 private:
    byte rx;
    byte tx;
 };

extern SercomSerial1 Serial2;

#else
#error "Serial2.h: currently only supports board with a second UART or using µC SAMD21G18A"
#endif
