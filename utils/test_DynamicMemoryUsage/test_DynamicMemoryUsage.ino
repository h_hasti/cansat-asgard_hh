/*
    This is a minimal sketch which is NOT intended to be run, only to be compiled
    in order to evaluate the memory usage when using the EEPROM_Bank class and all
    underlying libraries.
    It should be compiled with debugging off or configured in an operational setting.`
    Current results with DEBUG undefined and USE_ASSERTIONS defined in
    EEPROM_Bank.cpp & ExtEEPROM.cpp: (program mem / dynamic mem, in bytes)
      - USE_SERIAL          : 1430/184
      - USE_WIRE_LIB        : 3042/394
      - USE_EXT_EEPROM      : 3764/394 OK. 0 dyn mem.
      - USE_HW_SCANNER      : 7558/432 OK. Dyn. memory for required virtual methods
      - USE_ELAPSED_MILLIS  : 7646/432 OK. 0 dyn. mem.
      - USE_EEPROM_BANK     : 9466/442 OK. Dyn. memory for required virtual methods
      - USE_SDFAT_LIBRARY   : 17438/509
      - USE_SD_LOGGER       : 21886/535
      - USE_STORAGE_MGR     : 22332/535
      - USE_ISATIS_PROCESS  : >32k ?????
      - USE_GLOBALS         : 9676/570 ??? Why more program memory??

      TO BE CONTINUED.
      ?? Huge increase in program size between SdFat and SD_Logger...
*/

#define USE_SERIAL          1
#define USE_WIRE_LIB        2
#define USE_EXT_EEPROM      4
#define USE_HW_SCANNER      6
#define USE_ELAPSED_MILLIS  7
#define USE_EEPROM_BANK     8

#define USE_SDFAT_LIBRARY   14
#define USE_SD_LOGGER       15

#define USE_STORAGE_MGR     18
#define USE_ISATIS_PROCESS  19
#define USE_GLOBALS         20

#define STEP                USE_ISATIS_PROCESS

#if STEP >= USE_ISATIS_PROCESS
#include "IsatisAcquisitionProcess.h"
#include "AcquisitionProcess.h"
#endif
#if STEP >= USE_STORAGE_MGR
#include "IsatisStorageManager.h"
#include "StorageManager.h"
#endif
#if STEP >= USE_HW_SCANNER
#  include "HardwareScanner.h"
#endif
#if STEP >= USE_WIRE_LIB
#  include "Wire.h"
#endif
#if STEP >= USE_EXT_EEPROM
#  include "ExtEEPROM.h"
#endif
#if STEP >= USE_ELAPSED_MILLIS
#  include "elapsedMillis.h"
#endif
#if STEP >= USE_EEPROM_BANK
#  include "EEPROM_BankWriter.h"
#endif
#if STEP >= USE_SDFAT_LIBRARY
#include "SdFat.h"
#endif
#if STEP >= USE_SD_LOGGER
#include "SD_Logger.h"
#endif


#if STEP >= USE_GLOBALS
#include "IsatisDataRecord.h"
HardwareScanner gHW;
EEPROM_BankWriter gBank;
SD_Logger gLog;
IsatisDataRecord gDataRecord;
#endif

void setup() {
  long data;
  long*dataPtr = NULL;
#if STEP>=USE_SERIAL
  Serial.begin(9600);
  //Serial.print(1);
  //Serial.print(2);
#endif

#if STEP >= USE_WIRE_LIB
  Wire.begin();
  Wire.write(10);
  Wire.beginTransmission(10);
  Wire.endTransmission();
  Wire.requestFrom(10, 3);
  Wire.available();
  Wire.read();
#endif

#if STEP >= USE_HW_SCANNER
  HardwareScanner hw;
  hw.init();
  hw.printFullDiagnostic(Serial);
#endif

#if STEP >= USE_ELAPSED_MILLIS
  elapsedMillis em = 0;
  if (em > 10) em = 0;
#endif

#if STEP >= USE_EXT_EEPROM
  ExtEEPROM::writeByte(12, 2, (byte) 1);
  ExtEEPROM::readByte(12, 2, (byte&) data);
  ExtEEPROM::writeData(12, 2, (byte*) &data, 2);
  ExtEEPROM::readData(12, 2, (byte*) &data, 3);
#endif

#if STEP >= USE_EEPROM_BANK
  EEPROM_BankWriter bank(5);
  bank.init(0xaaaa, hw, 50);
  bank.storeData((byte*) &data, sizeof(data));
  bank.getTotalSize();
  bank.memoryFull();
  bank.doIdle();
#endif

#if STEP >= USE_SDFAT_LIBRARY
  SdFat SD;
  SD.begin(10);
  File f = SD.open("eee", FILE_READ);
  unsigned long size = f.fileSize();
  if (SD.exists("ddfde")) {
    f.println("test");
  }
  const unsigned long overFlowLimit = UINT_MAX / 512;
  unsigned long  freeBytes = SD.vol()->freeClusterCount();
  freeBytes *= SD.vol()->blocksPerCluster();
  f.println(freeBytes);
  f.flush();
  f.close();
#endif

#if STEP >= USE_SD_LOGGER
  SD_Logger logger;
  logger.init("azer", 10, "", 100);
  logger.log("essai");
  String str = logger.fileName();
  logger.doIdle();
#endif

#if STEP >= USE_STORAGE_MGR
  IsatisStorageManager sm(2,3);
  sm.init();
  IsatisDataRecord dr;
  sm.storeIsatisRecord(dr);
#endif

#if STEP >= USE_ISATIS_PROCESS
  IsatisAcquisitionProcess process;
 // process.init();
 // process.run();
#endif

#if STEP >= USE_GLOBALS
  gHW.init();
  gBank.init(0xaaaa, hw, 50);
  gBank.storeOneRecord((const byte * ) &gDataRecord, 20);
#endif
}

void loop() {
  // put your main code here, to run repeatedly:

}
