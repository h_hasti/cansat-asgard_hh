/*
 * HardwareScanner
 * 
 * This utility just creates a HardwareScanner and prints diagnostic on the Serial output.
 * Only information that can be automatically discovered is shown. 
 * 
 */

#include "HardwareScanner.h" // Include this first to have libraries linked in the right order. 
 
 void setup() {
  Serial.begin(115200);
  while (!Serial) ;

  Serial.println("Running Hardware Scanner...");
  Serial.flush();
  HardwareScanner hw;
  hw.init(1,127);
  hw.printFullDiagnostic(Serial);
}

void loop() {
  // put your main code here, to run repeatedly:

}
